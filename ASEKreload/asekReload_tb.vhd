library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity asekReload_tb is
end;

architecture asekReload_tb_arq of asekReload_tb is

	component asekReload is
		port
		(
			clk		:	in	std_logic;
			rst_n		:	in	std_logic;
			
			--Uart connections to PC
			uart_rx	:	in		std_logic;
			uart_tx	:	out	std_logic;
			
			uart_rx_eco : out std_logic;
			test_0	:	out std_logic;
			test_1	:	out std_logic;
			test_2	:	out std_logic;
			test_3	:	out std_logic;
			
			--I2C connections to DUT
			i2c_sda	:	inout	std_logic;
			i2c_scl	:	inout	std_logic;
			
			--SPI connections to DUT
			spi_miso    : IN     STD_LOGIC;                             		--master in, slave out
			spi_sclk    : BUFFER STD_LOGIC;                             		--spi clock
			--spi_ss_n    : BUFFER STD_LOGIC_VECTOR(SLAVES_NUMBER-1 DOWNTO 0);
			spi_ss_n    : BUFFER STD_LOGIC;												--slave select
			spi_mosi    : OUT    STD_LOGIC												--master out, slave in
		);
	end component;
	
	signal clk_tb			:	std_logic := '0';
	signal rst_n_tb		:	std_logic := '0';
	signal uart_rx_tb		:	std_logic := '1';
	signal uart_tx_tb		:	std_logic;
	signal i2c_sda_tb		:	std_logic := '0';
	signal i2c_scl_tb		:	std_logic;
	signal spi_miso_tb	:	std_logic := '0';
	signal spi_sclk_tb	:	std_logic := '0';
	signal spi_ss_tb		:	std_logic := '0';
	signal spi_mosi_tb	:	std_logic;
	
	signal uart_rx_eco_tb	:	std_logic;
	signal test_1_tb		:	std_logic;
	signal test_2_tb		:	std_logic;
	signal test_0_tb		:	std_logic;
	signal test_3_tb		:	std_logic;
	
begin
	clk_tb <= not clk_tb after 10 ns;
	rst_n_tb <= '1' after 200 ns;

	DUT : asekReload
		port map
		(
			clk		=> clk_tb,
			rst_n		=> rst_n_tb,
			uart_rx	=> uart_rx_tb,
			uart_tx	=> uart_tx_tb,
			uart_rx_eco => uart_rx_eco_tb,
			test_0	=> test_0_tb,
			test_1	=> test_1_tb,
			test_2	=> test_2_tb,
			test_3	=> test_3_tb,
			i2c_sda	=> i2c_sda_tb,
			i2c_scl	=> i2c_scl_tb,
			spi_miso => spi_miso_tb,
			spi_sclk => spi_sclk_tb,
			spi_ss_n => spi_ss_tb,
			spi_mosi => spi_mosi_tb
		);
		
	test : process
		begin
		
			
			wait for 200000 ns;
			
			--Test SPI
			uart_rx_tb <= '0'; --bit start
			wait for 104167 ns;
			-- receive i = x"69"
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1'; --bit stop
			
			wait for 200000 ns;
			
			uart_rx_tb <= '0'; --bit start
			wait for 104167 ns;
			-- receive s = x"73"
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1'; --bit stop
			
			wait for 200000 ns;
			
			uart_rx_tb <= '0'; --bit start
			wait for 104167 ns;
			-- receive s = x"73"
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1'; --bit stop
			
			wait for 200000 ns;
			
			uart_rx_tb <= '0'; --bit start
			wait for 104167 ns;
			-- receive s = x"73"
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1'; --bit stop
			
			wait for 1000000 ns;
			
			--test i2c
			uart_rx_tb <= '0'; --bit start
			wait for 104167 ns;
			-- receive s = x"69"
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1'; --bit stop
			
			wait for 200000 ns;
			
						uart_rx_tb <= '0'; --bit start
			wait for 104167 ns;
			-- receive s = x"69"
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1'; --bit stop
			
			wait for 200000 ns;
			
						uart_rx_tb <= '0'; --bit start
			wait for 104167 ns;
			-- receive s = x"69"
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1'; --bit stop
			
			wait for 200000 ns;
			
						uart_rx_tb <= '0'; --bit start
			wait for 104167 ns;
			-- receive s = x"69"
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '1';
			wait for 104167 ns;
			uart_rx_tb <= '0';
			wait for 104167 ns;
			uart_rx_tb <= '1'; --bit stop
			
			wait for 200000 ns;
			
			wait for 1000000 ns;
			
	end process;

end;