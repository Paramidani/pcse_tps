library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library altera; 
use altera.altera_primitives_components.all;

entity DetecFlanco is
	port
	(
		clk			: in  std_logic;
		rx				: in  std_logic;
		flanco_neg	: out std_logic
	);
end DetecFlanco;


architecture DetArch of DetecFlanco is
	signal q1, q2		: std_logic;
	signal logic_one	: std_logic;

begin

	logic_one	<= '1';
	
	dff_instance_1 : DFF
	port map (
			d		=> rx, 
			clk	=> clk, 
			clrn	=> logic_one,
			prn	=> logic_one,
			q		=> q1
	);

	dff_instance_2 : DFF
	port map (
			d		=> q1, 
			clk	=> clk, 
			clrn	=> logic_one,
			prn	=> logic_one,
			q		=> q2
	);
			
	flanco_neg <= (q1 and (not(q2)));
	
end DetArch;

