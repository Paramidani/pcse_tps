library IEEE;
use IEEE.std_logic_1164.all;

entity asekReload is
	port
	(
		clk		:	in	std_logic;
		rst_n		:	in	std_logic;
		
		--Uart connections to PC
		uart_rx	:	in		std_logic;
		uart_tx	:	out	std_logic;
		
		uart_rx_eco : out std_logic;
		test_1	:	out std_logic;
		test_2	:	out std_logic;
		
		--I2C connections to DUT
		i2c_sda	:	inout	std_logic;
		i2c_scl	:	inout	std_logic;
		
		--SPI connections to DUT
		spi_miso    : IN     STD_LOGIC;                             		--master in, slave out
		spi_sclk    : BUFFER STD_LOGIC;                             		--spi clock
		--spi_ss_n    : BUFFER STD_LOGIC_VECTOR(SLAVES_NUMBER-1 DOWNTO 0);
		spi_ss_n    : BUFFER STD_LOGIC;												--slave select
		spi_mosi    : OUT    STD_LOGIC												--master out, slave in
	);
end asekReload;



architecture asekReload_arq of asekReload is

	COMPONENT uart
		GENERIC(
			clk_freq		:	INTEGER		:= 50_000_000;	--frequency of system clock in Hertz
			baud_rate	:	INTEGER		:= 19_200;		--data link baud rate in bits/second
			os_rate		:	INTEGER		:= 16;			--oversampling rate to find center of receive bits (in samples per baud period)
			d_width		:	INTEGER		:= 8; 			--data bus width
			parity		:	INTEGER		:= 1;				--0 for no parity, 1 for parity
			parity_eo	:	STD_LOGIC	:= '0');			--'0' for even, '1' for odd parity
		PORT(
			clk		:	IN		STD_LOGIC;										--system clock
			reset_n	:	IN		STD_LOGIC;										--ascynchronous reset
			tx_ena	:	IN		STD_LOGIC;										--initiate transmission
			tx_data	:	IN		STD_LOGIC_VECTOR(d_width-1 DOWNTO 0);  --data to transmit
			rx			:	IN		STD_LOGIC;										--receive pin
			rx_busy	:	OUT	STD_LOGIC;										--data reception in progress
			rx_error	:	OUT	STD_LOGIC;										--start, parity, or stop bit error detected
			rx_data	:	OUT	STD_LOGIC_VECTOR(d_width-1 DOWNTO 0);	--data received
			tx_busy	:	OUT	STD_LOGIC;  									--transmission in progress
			tx			:	OUT	STD_LOGIC);										--transmit pin
	END COMPONENT;

	COMPONENT spi_master
	  GENERIC(
		 slaves  : INTEGER := 4;  --number of spi slaves
		 d_width : INTEGER := 2); --data bus width
	  PORT(
		 clock   : IN     STD_LOGIC;                             --system clock
		 reset_n : IN     STD_LOGIC;                             --asynchronous reset
		 enable  : IN     STD_LOGIC;                             --initiate transaction
		 cpol    : IN     STD_LOGIC;                             --spi clock polarity
		 cpha    : IN     STD_LOGIC;                             --spi clock phase
		 cont    : IN     STD_LOGIC;                             --continuous mode command
		 clk_div : IN     INTEGER;                               --system clock cycles per 1/2 period of sclk
		 addr    : IN     INTEGER;                               --address of slave
		 tx_data : IN     STD_LOGIC_VECTOR(d_width-1 DOWNTO 0);  --data to transmit
		 miso    : IN     STD_LOGIC;                             --master in, slave out
		 sclk    : BUFFER STD_LOGIC;                             --spi clock
		 ss_n    : BUFFER STD_LOGIC_VECTOR(slaves-1 DOWNTO 0);   --slave select
		 mosi    : OUT    STD_LOGIC;                             --master out, slave in
		 busy    : OUT    STD_LOGIC;                             --busy / data ready signal
		 rx_data : OUT    STD_LOGIC_VECTOR(d_width-1 DOWNTO 0)); --data received
	END COMPONENT;
	
	COMPONENT i2c_master
	  GENERIC(
		 input_clk : INTEGER := 50_000_000; --input clock speed from user logic in Hz
		 bus_clk   : INTEGER := 400_000);   --speed the i2c bus (scl) will run at in Hz
	  PORT(
		 clk       : IN     STD_LOGIC;                    --system clock
		 reset_n   : IN     STD_LOGIC;                    --active low reset
		 ena       : IN     STD_LOGIC;                    --latch in command
		 addr      : IN     STD_LOGIC_VECTOR(6 DOWNTO 0); --address of target slave
		 rw        : IN     STD_LOGIC;                    --'0' is write, '1' is read
		 data_wr   : IN     STD_LOGIC_VECTOR(7 DOWNTO 0); --data to write to slave
		 busy      : OUT    STD_LOGIC;                    --indicates transaction in progress
		 data_rd   : OUT    STD_LOGIC_VECTOR(7 DOWNTO 0); --data read from slave
		 ack_error : BUFFER STD_LOGIC;                    --flag if improper acknowledge from slave
		 sda       : INOUT  STD_LOGIC;                    --serial data output of i2c bus
		 scl       : INOUT  STD_LOGIC);                   --serial clock output of i2c bus
	END COMPONENT;

--	component DetecFlanco
--		port
--		(
--			clk			: in  std_logic;
--			rx				: in  std_logic;
--			flanco_neg	: out std_logic
--		);
--	end component;

-- Constant values
	constant UART_SYS_CLK_FREQ	:	integer := 50_000_000;
	constant UART_BAUD_RATE		:	integer := 9600;
	constant UART_OS_RATE		:	integer := 16;
	constant UART_DATA_WIDTH	:	integer := 8;
	constant UART_PARITY_NONE	:	integer := 0;
	constant UART_PERITY_EVEN	:	std_logic := '0';
	constant SPI_SLAVES_NUMBER	:	integer := 2;
	constant SPI_DATA_WIDTH		:	integer := 16;
	constant SPI_CPOL_ZERO		:	std_logic := '0';
	constant SPI_CPOL_ONE		:	std_logic := '1';
	constant SPI_CPHA_ZERO		:	std_logic := '0';
	constant SPI_CPHA_ONE		:	std_logic := '1';
	constant SPI_CLK_DIV			:	integer := 4;
	constant SPI_CONTINUOS_NO	:	std_logic := '0';
	constant SPI_CONTINUOS_YES	:	std_logic := '1';
	constant	SPI_SLAVE_ADDR		:	integer := 0;				
	constant I2C_SYS_CLK_FREQ	:	integer := 50_000_000;
	constant I2C_BUS_CLK_FREQ	:	integer := 400_000;
	constant I2C_DATA_WIDTH		:	integer := 8;
	constant I2C_ADDR_WIDTH		:	integer := 7;
	constant I2C_SAVE_ADDR		:	std_logic_vector(I2C_ADDR_WIDTH-1 downto 0) := "1010101";
	constant I2C_WRITE_MODE		:	std_logic := '0';
	constant I2C_READ_MODE		:	std_logic := '1';
	
	constant MSG_DATA_WIDTH		:	integer := 32; --1 byte: protocol index, 1 byte: reg addres, 2 bytes: data
	constant UART_NRO_BYTES		:	integer := MSG_DATA_WIDTH/UART_DATA_WIDTH;
	
-- Definición del vector de estados
	type fsm_control is (READY, DECODING_UART, WRITING_SPI, WRITING_I2C, SENDING_FBACK);--, CONFIG_DATA_WIDTH);
	
-- Signals
	signal current_state		:	fsm_control;
	signal next_state			:	fsm_control;
	signal s_spi_ss_n			:	std_logic_vector(SPI_SLAVES_NUMBER-1 downto 0);
	signal s_uart_tx_ena		:	std_logic;
	signal b_uart_tx_data	:	std_logic_vector(UART_DATA_WIDTH-1 downto 0);
	signal s_uart_rx_busy	:	std_logic;
	signal s_uart_rx_error	:	std_logic;
	signal b_uart_rx_data	:	std_logic_vector(UART_DATA_WIDTH-1 downto 0);
	signal s_uart_tx_busy	:	std_logic;
	signal s_spi_enable		:	std_logic;
	signal b_spi_tx_data		:	std_logic_vector(SPI_DATA_WIDTH-1 downto 0);
	signal s_spi_busy			:	std_logic;
	signal b_spi_rx_data		:	std_logic_vector(SPI_DATA_WIDTH-1 downto 0);
	signal s_i2c_enable		:	std_logic;	
	signal b_i2c_addr			:	std_logic_vector(I2C_ADDR_WIDTH-1 downto 0);
	signal s_i2c_rw			:	std_logic;
	signal b_i2c_data_wr		:	std_logic_vector(I2C_DATA_WIDTH-1 downto 0);
	signal s_i2c_busy			:	std_logic;
	signal b_i2c_data_rd		:	std_logic_vector(I2C_DATA_WIDTH-1 downto 0);
	signal s_i2c_ack_error	:	std_logic;
	
	--signal s_uart_rx_busy_flanco_neg	:	std_logic;
	signal s_uart_rx_busy_prev	:	std_logic;
	signal s_spi_busy_prev		:	std_logic;
	signal s_i2c_busy_prev		:	std_logic;
	
	signal rx_message			:	std_logic_vector(MSG_DATA_WIDTH-1 downto 0);
	
	
begin

	--For testing only
	uart_rx_eco <= uart_rx and rst_n;
	test_1	<= s_uart_rx_error;--'1' when current_state=DECODING_UART else '0';
	test_2	<= '1' when current_state=DECODING_UART else '0';

	spi_ss_n <= s_spi_ss_n(0); --Only one slave is used for spi
	b_i2c_addr <= I2C_SAVE_ADDR;	--Hardcoded 
	s_i2c_rw <= I2C_WRITE_MODE;

	uart_i : uart
		generic map(
			clk_freq		=> UART_SYS_CLK_FREQ,	--frequency of system clock in Hertz
			baud_rate	=> UART_BAUD_RATE,		--data link baud rate in bits/second
			os_rate		=> UART_OS_RATE,			--oversampling rate to find center of receive bits (in samples per baud period)
			d_width		=> UART_DATA_WIDTH,		--data bus width
			parity		=> UART_PARITY_NONE,		--0 for no parity, 1 for parity
			parity_eo	=> UART_PERITY_EVEN		--'0' for even, '1' for odd parity
		)
		port map(
			clk		=> clk,					--system clock
			reset_n	=> rst_n,				--ascynchronous reset
			tx_ena	=> s_uart_tx_ena,		--initiate transmission
			tx_data	=> b_uart_tx_data,	--data to transmit
			rx			=> uart_rx,				--receive pin
			rx_busy	=> s_uart_rx_busy,	--data reception in progress
			rx_error	=> s_uart_rx_error,	--start, parity, or stop bit error detected
			rx_data	=> b_uart_rx_data,	--data received
			tx_busy	=> s_uart_tx_busy,	--transmission in progress
			tx			=> uart_tx				--transmit pin
		);
		
	spi_master_i : spi_master
		generic map(
			slaves  => SPI_SLAVES_NUMBER,	--number of spi slaves
			d_width => SPI_DATA_WIDTH		--data bus width
		)
		port map(
			clock   => clk,					--system clock
			reset_n => rst_n,					--asynchronous reset
			enable  => s_spi_enable,		--initiate transaction
			cpol    => SPI_CPOL_ZERO,		--spi clock polarity
			cpha    => SPI_CPHA_ZERO,		--spi clock phase
			cont    => SPI_CONTINUOS_NO,	--continuous mode command
			clk_div => SPI_CLK_DIV,			--system clock cycles per 1/2 period of sclk
			addr    => SPI_SLAVE_ADDR,		--address of slave
			tx_data => b_spi_tx_data,		--data to transmit
			miso    => spi_miso,				--master in, slave out
			sclk    => spi_sclk,				--spi clock
			ss_n    => s_spi_ss_n,			--slave select
			mosi    => spi_mosi,				--master out, slave in
			busy    => s_spi_busy,			--busy / data ready signal
			rx_data => b_spi_rx_data		--data received	
		);
		

		
	i2c_master_i : i2c_master
		generic map(
			input_clk => I2C_SYS_CLK_FREQ,	--input clock speed from user logic in Hz
			bus_clk   => I2C_BUS_CLK_FREQ		--speed the i2c bus (scl) will run at in Hz		
		)
		port map(
			clk       => clk,					--system clock
			reset_n   => rst_n,				--active low reset
			ena       => s_i2c_enable,		--latch in command
			addr      => b_i2c_addr,		--address of target slave
			rw        => s_i2c_rw,			--'0' is write, '1' is read
			data_wr   => b_i2c_data_wr,	--data to write to slave
			busy      => s_i2c_busy,		--indicates transaction in progress
			data_rd   => b_i2c_data_rd,	--data read from slave
			ack_error => s_i2c_ack_error,	--flag if improper acknowledge from slave
			sda       => i2c_sda,			--serial data output of i2c bus
			scl       => i2c_scl				--serial clock output of i2c bus
		);
		
--	uart_rx_flanco_i : DetecFlanco
--		port map(
--			clk			=> clk,
--			rx				=> s_uart_rx_busy,
--			flanco_neg	=> s_uart_rx_busy_flanco_neg
--		);


-- FSM control
--		type fsm_control is 
--							(READY, 
--							DECODING_UART, 
--							WRITING_SPI,
--							WRITING_I2C,
--							SENDING_FBACK);

	process(clk, rst_n)
	begin
		if(rst_n = '0') then
			current_state <= READY;
		elsif rising_edge(clk) then 
			current_state	<=	next_state;
		end if;
	end process;

-- TODO: agregar chequeo de errores en las comunicaciones
--	TODO: agregar time out en las comunicaciones
	fsm : process(current_state, s_uart_rx_busy, b_uart_rx_data, s_spi_busy, s_i2c_busy, s_uart_tx_busy, rx_message, s_uart_rx_busy_prev, s_spi_busy_prev, s_i2c_busy_prev)
		variable uart_rx_cnt	: integer := UART_NRO_BYTES;
		variable spi_tx_cnt	: integer := 2;
		variable i2c_tx_cnt	: integer := 3;
		
	begin
		case current_state is
			when READY =>
				s_spi_enable <= '0';
				s_i2c_enable <= '0';
				s_uart_tx_ena <= '0';
				uart_rx_cnt	:= UART_NRO_BYTES;
				spi_tx_cnt	:= 2;
				i2c_tx_cnt	:= 3;
				b_uart_tx_data <= x"78"; -- Return "x"
				s_uart_rx_busy_prev <= s_uart_rx_busy;
				if (s_uart_rx_busy_prev = '0' and s_uart_rx_busy = '1') then
					next_state <= DECODING_UART;
					uart_rx_cnt	:= UART_NRO_BYTES;
				else
					next_state <= READY;
				end if;
				
			when DECODING_UART =>
				s_spi_enable <= '0';
				s_i2c_enable <= '0';
				s_uart_tx_ena <= '0';
				spi_tx_cnt	:= 2;
				i2c_tx_cnt	:= 3;
				b_uart_tx_data <= x"78"; -- Return "x"
				s_uart_rx_busy_prev <= s_uart_rx_busy;
				case uart_rx_cnt is
					when 4 =>	
						rx_message(31 downto 24)<= b_uart_rx_data;
						rx_message(23 downto 0)<= rx_message(23 downto 0);
						if (s_uart_rx_busy_prev = '0' and s_uart_rx_busy = '1') then
							uart_rx_cnt := uart_rx_cnt - 1;
						end if;
						next_state <= DECODING_UART;
					
					when 3 =>
						rx_message(31 downto 24)<=rx_message(31 downto 24);
						rx_message(23 downto 16)<= b_uart_rx_data;
						rx_message(15 downto 0)<= rx_message(15 downto 0);
						if (s_uart_rx_busy_prev = '0' and s_uart_rx_busy = '1') then
							uart_rx_cnt := uart_rx_cnt - 1;
						end if;
						next_state <= DECODING_UART;
					
					when 2 =>
						rx_message(31 downto 16)<=rx_message(31 downto 16);
						rx_message(15 downto 8)<= b_uart_rx_data;
						rx_message(7 downto 0)<=rx_message(7 downto 0);
						if (s_uart_rx_busy_prev = '0' and s_uart_rx_busy = '1') then
							uart_rx_cnt := uart_rx_cnt - 1;
						end if;
						next_state <= DECODING_UART;
					
					when 1 =>	
						rx_message(31 downto 8)<=rx_message(31 downto 8);
						rx_message(7 downto 0)<= b_uart_rx_data;
						if (s_uart_rx_busy_prev = '1' and s_uart_rx_busy = '0') then
							uart_rx_cnt := uart_rx_cnt - 1;
						end if;
						next_state <= DECODING_UART;
						
					when 0 =>
						rx_message(31 downto 0)<=rx_message(31 downto 0);
						if (rx_message((UART_NRO_BYTES*UART_DATA_WIDTH-1) downto ((UART_NRO_BYTES-1)*UART_DATA_WIDTH)) = x"73") then --if protocol="S"
							next_state <= WRITING_SPI;
							--s_spi_enable <= '1';
							uart_rx_cnt := UART_NRO_BYTES;
							spi_tx_cnt := 2;
						elsif (rx_message((UART_NRO_BYTES*UART_DATA_WIDTH-1) downto ((UART_NRO_BYTES-1)*UART_DATA_WIDTH)) = x"69") then --if protocol="I"
							next_state <= WRITING_I2C;
							--s_i2c_enable <= '1';
							uart_rx_cnt := UART_NRO_BYTES;
							i2c_tx_cnt := 3;
						else
							next_state <= READY;
							uart_rx_cnt := UART_NRO_BYTES;
						end if;
						
					when others =>
						rx_message(31 downto 0)<=rx_message(31 downto 0);
						next_state <= READY;
						uart_rx_cnt := UART_NRO_BYTES;
				end case;
				
			when WRITING_SPI =>
				rx_message(31 downto 0)<=rx_message(31 downto 0);
				s_i2c_enable <= '0';
				i2c_tx_cnt	:= 3;
				b_uart_tx_data <= x"78"; -- Return "x"
				s_uart_tx_ena <= '0';
				s_spi_busy_prev <= s_spi_busy;
				uart_rx_cnt	:= UART_NRO_BYTES;
				case spi_tx_cnt is
					when 2 =>
						-- cargo en el byte alto de b_spi_tx_data el reg address, rx_message(23 downto 16)
						b_spi_tx_data(15 downto 8) <= rx_message((MSG_DATA_WIDTH-UART_DATA_WIDTH-1) downto (MSG_DATA_WIDTH-2*UART_DATA_WIDTH));  
						-- cargo el primer dato a enviar en el byte bajo de b_spi_tx_data, rx_message(15 downto 8)
						b_spi_tx_data(7 downto 0) <= rx_message((MSG_DATA_WIDTH-2*UART_DATA_WIDTH-1) downto (MSG_DATA_WIDTH-3*UART_DATA_WIDTH));
						-- habilito la transacción spi
						s_spi_enable <= '1';
						next_state <= WRITING_SPI;
						if (s_spi_busy_prev = '1' and s_spi_busy = '0') then
							spi_tx_cnt := spi_tx_cnt - 1;
						end if;	
					when 1 =>
						-- cargo en el byte alto de b_spi_tx_data el reg address, rx_message(23 downto 16)
						b_spi_tx_data(15 downto 8) <= rx_message((MSG_DATA_WIDTH-UART_DATA_WIDTH-1) downto (MSG_DATA_WIDTH-2*UART_DATA_WIDTH));  
						-- cargo el segundo dato a enviar en el byte bajo de b_spi_tx_data, rx_message(7 downto 0)
						b_spi_tx_data(7 downto 0) <= rx_message((MSG_DATA_WIDTH-3*UART_DATA_WIDTH-1) downto (MSG_DATA_WIDTH-4*UART_DATA_WIDTH));
						-- habilito la transacción spi
						s_spi_enable <= '1';
						next_state <= WRITING_SPI;
						if (s_spi_busy_prev = '1' and s_spi_busy = '0') then
							--spi_tx_cnt := spi_tx_cnt - 1;
							--s_spi_enable <= '0';
							s_spi_enable <= '0';
							next_state <= SENDING_FBACK;
							s_uart_tx_ena <= '1';
							b_uart_tx_data <= X"73"; -- Return "s"
							spi_tx_cnt := 2;
						end if;
					when others =>
						s_spi_enable <= '0';
						next_state <= READY;
						s_uart_tx_ena <= '0';
						b_uart_tx_data <= X"78"; -- Return "s"
						spi_tx_cnt := 2;
						
				end case;		
			
			when WRITING_I2C =>
				rx_message(31 downto 0)<=rx_message(31 downto 0);
				s_spi_enable <= '0';
				s_i2c_busy_prev <= s_i2c_busy;
				s_uart_tx_ena <= '0';
				b_uart_tx_data <= x"78"; -- Return "x"
				uart_rx_cnt	:= UART_NRO_BYTES;
				spi_tx_cnt	:= 2;
				case i2c_tx_cnt is
					when 3 => -- sending reg address
						b_i2c_data_wr <= rx_message((MSG_DATA_WIDTH-UART_DATA_WIDTH-1) downto (MSG_DATA_WIDTH-2*UART_DATA_WIDTH));
						s_i2c_enable <= '1';
						next_state <= WRITING_I2C;
						if (s_i2c_busy_prev = '1' and s_i2c_busy = '0') then
							i2c_tx_cnt := i2c_tx_cnt - 1;
						end if;
					when 2 => --sending first data byte
						b_i2c_data_wr <= rx_message((MSG_DATA_WIDTH-2*UART_DATA_WIDTH-1) downto (MSG_DATA_WIDTH-3*UART_DATA_WIDTH));
						s_i2c_enable <= '1';
						next_state <= WRITING_I2C;
						if (s_i2c_busy_prev = '1' and s_i2c_busy = '0') then
							i2c_tx_cnt := i2c_tx_cnt - 1;
						end if;
					when 1 => --sending second data byte
						b_i2c_data_wr <= rx_message((MSG_DATA_WIDTH-3*UART_DATA_WIDTH-1) downto (MSG_DATA_WIDTH-4*UART_DATA_WIDTH));
						s_i2c_enable <= '0';
						next_state <= WRITING_I2C;
						if (s_i2c_busy_prev = '1' and s_i2c_busy = '0') then
							i2c_tx_cnt := i2c_tx_cnt - 1;
							--s_i2c_enable <= '0';
						end if;
					when 0 =>
						s_i2c_enable <= '0';
						i2c_tx_cnt := 3;
						next_state <= SENDING_FBACK;
						s_uart_tx_ena <= '1';
						b_uart_tx_data <= X"69"; -- Return "i"	
					when others =>
						s_i2c_enable <= '0';
						i2c_tx_cnt := 3;
						next_state <= READY;
						s_uart_tx_ena <= '0';
						b_uart_tx_data <= x"78"; -- Return "x"					
				end case;
			
			when SENDING_FBACK =>
				rx_message(31 downto 0)<=rx_message(31 downto 0);
				s_spi_enable <= '0';
				s_i2c_enable <= '0';
				s_uart_tx_ena <= '0';
				uart_rx_cnt	:= UART_NRO_BYTES;
				spi_tx_cnt	:= 2;
				i2c_tx_cnt	:= 3;
				if (s_uart_tx_busy = '0') then
					next_state <= READY;
				else
					next_state <= SENDING_FBACK;
				end if;
			
			When others =>
				rx_message(31 downto 0)<=rx_message(31 downto 0);
				s_spi_enable <= '0';
				s_i2c_enable <= '0';
				s_uart_tx_ena <= '0';
				uart_rx_cnt	:= UART_NRO_BYTES;
				spi_tx_cnt	:= 2;
				i2c_tx_cnt	:= 3;
				b_uart_tx_data <= x"78"; -- Return "x"	
				next_state <= READY;
				
		end case;
	end process;
end asekReload_arq;

