onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /asekreload_tb/uart_rx_tb
add wave -noupdate /asekreload_tb/uart_tx_tb
add wave -noupdate /asekreload_tb/rst_n_tb
add wave -noupdate /asekreload_tb/spi_miso_tb
add wave -noupdate /asekreload_tb/spi_sclk_tb
add wave -noupdate /asekreload_tb/spi_ss_tb
add wave -noupdate /asekreload_tb/DUT/current_state
add wave -noupdate /asekreload_tb/DUT/rx_message
add wave -noupdate /asekreload_tb/DUT/s_uart_rx_busy
add wave -noupdate /asekreload_tb/DUT/s_spi_busy
add wave -noupdate /asekreload_tb/DUT/fsm/uart_rx_cnt
add wave -noupdate /asekreload_tb/DUT/fsm/spi_tx_cnt
add wave -noupdate /asekreload_tb/DUT/fsm/i2c_tx_cnt
add wave -noupdate /asekreload_tb/i2c_scl_tb
add wave -noupdate /asekreload_tb/i2c_sda_tb
add wave -noupdate /asekreload_tb/DUT/s_i2c_busy
add wave -noupdate /asekreload_tb/DUT/s_i2c_enable
add wave -noupdate /asekreload_tb/DUT/s_i2c_ack_error
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {18957370 ns} 0} {{Cursor 2} {4838432 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 204
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {21 ms}
